# Todo API Documentation

This API provides endpoints to manage users, projects, and tasks for a todo list application.

## User Management

### Create User

```
https://todo-api-project-zuber.onrender.com/api/user
```

- Route: `/api/user`
- Method: POST
- Sample Request:
  ```json
  {
    "name": "zuber123",
    "email": "zuber@test.com",
    "password": "12345",
  }
  ```
- Sample Response:
  ```json
  {
    "success": true,
    "data": {
        "username": "zuber123",
        "email": "zuber123@test.com"
    }
  }
  ```
- Description: This route allows users to sign up by providing their name, email and password.

### User Login

```
https://todo-api-project-zuber.onrender.com/api/user/login
```

- Route: `/api/user/login`
- Method: POST
- Sample Request:
  ```json
  { 
    "email": "zuber@test.com",
    "password": "12345"
  }
  ```
- Sample Response:
  ```json
    {
        "success": true,
        "data": {
            "username": "zuber",
            "email": "zuber@test.com",
            "token": "jwt token"
        }
    }
  ```
- Description: Users can log in using their email and password to receive an access token.

### Get User Details by Username

```
https://todo-api-project-zuber.onrender.com/api/user/username
```

- Route: `/api/user/:username`
- Method: GET
- Sample Response:
  ```json
    {
        "success": true,
        "data": {
            "username": "username",
            "email": "username@test.com"
        }
    }
  ```
- Description: Retrieve user details by providing the username.


### Update User Details by Username

```
https://todo-api-project-zuber.onrender.com/api/user/username
```

- Route: `/api/user/:username`
- Method: PUT
- Sample Request:
  ```json
  { 
    "email": "zuber@test.com"
  }
  ```
- Sample Response:
  ```json
    {
        "success": true,
        "message": "User updated successfully."
    }
  ```
- Description: Update user email by providing the username.


## Project Management

### Create Project

```
https://todo-api-project-zuber.onrender.com/api/project
```

- Route: `/api/project`
- Method: POST
- Headers: `Authorization:{jwt token}`
- Sample Request:
  ```json
  {
    "title": "Sample Project",
    "description": "This is a sample project."
  }
  ```
- Sample Response:
  ```json
    {
        "success": true,
        "data": {
            "title": "Sample Project",
            "description": "This is a sample project.",
            "completed": false,
            "user_id": 1,
            "id": 1
        }
    }
  ```
- Description: Create a new project with a title and description.



### Get All Projects Details

```
https://todo-api-project-zuber.onrender.com/api/project
```

- Route: `/api/project`
- Method: GET
- Headers: `Authorization:{jwt token}`
- Sample Response:
  ```json
    {
        "success": true,
        "data": [
            {
                "id": 1,
                "title": "Sample Project",
                "description": "This is a sample project.",
                "completed": false,
                "user_id": 1,
                "created_at": "2023-08-18T13:23:33.414Z",
                "updated_at": "2023-08-18T13:23:33.414Z",
                "task": []
            }
        ]
    }
  ```
- Description: Retrieve details of all projects.


### Get Single Project Details

```
https://todo-api-project-zuber.onrender.com/api/project/project_id
```

- Route: `/api/project/:project_id`
- Method: GET
- Headers: `Authorization:{jwt token}`
- Sample Response:
  ```json
    {
        "success": true,
        "data": {
            "id": 1,
            "title": "Sample Project",
            "description": "This is a sample project.",
            "completed": false,
            "user_id": 1,
            "created_at": "2023-08-18T13:23:33.414Z",
            "updated_at": "2023-08-18T13:23:33.414Z",
            "task": []
        }
    }
  ```
- Description: Retrieve details of a specific project by providing its ID.


### Update Project Details

```
https://todo-api-project-zuber.onrender.com/api/project/project_id
```

- Route: `/api/project/:project_id`
- Method: PUT
- Headers: `Authorization:{jwt token}`
- Sample Request:
  ```json
    {
        "title": "Sample Project 1",
        "description": "Sample Project 1 description",
        "completed": true
    }
  ```
- Sample Response:
  ```json
    {
        "success": true,
        "message": "Project updated successfully."
    }
  ```
- Description: Update the title and/or description and/or completed of a project.

### Delete Project

```
https://todo-api-project-zuber.onrender.com/api/project/project_id
```

- Route: `/api/project/:project_id`
- Method: DELETE
- Headers: `Authorization:{jwt token}`
- Sample Response:
  ```json
    {
        "success": true,
        "message": "Project deleted successfully."
    }
  ```
- Description: Delete a project by providing its ID.

## Task Management

### Create Task

```
https://todo-api-project-zuber.onrender.com/api/task
```

- Route: `/api/task`
- Method: POST
- Headers: `Authorization: {jwt token}`
- Sample Request:
  ```json
  {
    "title": "Sample Task",
    "project_id": 1
  }
  ```
- Sample Response:
  ```json
    {
        "success": true,
        "data": {
            "title": "Sample Task",
            "completed": false,
            "project_id": 1,
            "id": 1
        }
    }
  ```
- Description: Create a new task within a specific project.

### get all Task Details

```
https://todo-api-project-zuber.onrender.com/api/task
```

- Route: `/api/task`
- Method: GET
- Headers: `Authorization: {jwt token}`
- Sample Response:
  ```json
    {
        "success": true,
        "data": [
            {
                "id": 1,
                "title": "Sample Task",
                "completed": false,
                "project_id": 1,
                "created_at": "2023-08-18T17:14:58.404Z",
                "updated_at": "2023-08-18T17:14:58.404Z"
            }
        ]
    }
  ```
- Description: Retrieve details of all task.


### get a single Task Details

```
https://todo-api-project-zuber.onrender.com/api/task/task_id
```

- Route: `/api/task/:task_id`
- Method: GET
- Headers: `Authorization: {jwt token}`
- Sample Response:
  ```json
    {
        "success": true,
        "data": {
            "id": 1,
            "title": "Sample Task",
            "completed": false,
            "project_id": 1,
            "created_at": "2023-08-18T17:14:58.404Z",
            "updated_at": "2023-08-18T17:14:58.404Z"
        }
    }
  ```
- Description: Retrieve detail of a single tasks with specific task id.


### Update Task Details

```
https://todo-api-project-zuber.onrender.com/api/task/task_id
```

- Route: `/api/task/:task_id`
- Method: PUT
- Headers: `Authorization: {jwt token}`
- Sample Request:
  ```json
  {
    "title": "Updated Task Title",
    "completed": true
  }
  ```
- Sample Response:
  ```json
    {
        "success": true,
        "message": "Task updated successfuly."
    }
  ```
- Description: Update the title and/or completed of a task.

### Delete Task

```
https://todo-api-project-zuber.onrender.com/api/task/task_id
```

- Route: `/api/task/:task_id`
- Method: DELETE
- Headers: `Authorization: {jwt token}`
- Sample Response:
  ```json
    {
        "success": true,
        "message": "Task deleted successfuly."
    }
  ```
- Description: Delete a task by providing its ID.

