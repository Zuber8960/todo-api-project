const express = require("express");
const app = express();
app.use(express.json());

const { PORT } = require("./config");

const dbSetup = require("./src/db/db-setup");
dbSetup();

const userRouter = require("./src/routes/user");
const projectRouter = require("./src/routes/project");
const taskRouter = require("./src/routes/task");
const errorController = require("./src/controllers/error");

app.use("/api/user", userRouter);
app.use("/api/project", projectRouter);
app.use("/api/task", taskRouter);

app.use(errorController.error);

app.listen(PORT, () => {
  console.log(`server is running`);
});
