const Project = require("../db/models/project");
const Task = require("../db/models/task");

exports.createTask = async (req, res) => {
  try {
    const { project_id, title } = req.body;
    const projectData = await Project.query().findById(project_id);
    if (!projectData) {
      return res.status(400).json({
        success: false,
        message:
          "Invalid Project ID: The project you are trying to access does not exist.",
      });
    } else {
      if (req.user.id === projectData.user_id) {
        const data = await Task.query().insert({
          title: title,
          completed: false,
          project_id: project_id,
        });
        await Project.query().findById(project_id).patch({
          completed: false,
        });
        return res.status(200).json({ success: true, data: data });
      } else {
        return res.status(400).json({
          success: false,
          message: "Access Denied: You are not authorized.",
        });
      }
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

exports.getAllTasks = async (req, res) => {
  try {
    const tasks = await Task.query()
      .joinRelated("project")
      .where("project.user_id", "=", req.user.id);
    console.log(tasks);
    if (tasks.length === 0) {
      return res
        .status(400)
        .json({
          success: false,
          message: "No Task Found: Please create task.",
        });
    } else {
      return res.status(200).json({ success: true, data: tasks });
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

exports.getATask = async (req, res) => {
  try {
    const { id } = req.params;
    const task = await Task.query().findById(id).withGraphFetched("project");
    if (!task) {
      return res
        .status(400)
        .json({ success: false, message: "Task does not exist !" });
    } else {
      if (req.user.id === task.project.user_id) {
        return res.status(200).json({ success: true, data: task });
      } else {
        return res.status(400).json({
          success: false,
          message: "Access Denied: You are not authorized.",
        });
      }
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

exports.updateTask = async (req, res) => {
  try {
    const { id } = req.params;
    const { title, completed } = req.body;
    const task = await Task.query().findById(id).withGraphFetched("project");
    if (!task) {
      return res
        .status(400)
        .json({ success: false, message: "Task does not exist !" });
    } else {
      if (req.user.id === task.project.user_id) {
        await Task.query().findById(id).patch({
          title: title,
          completed: completed,
        });
        await updateProjectWhenUpdateTask(task.project_id);
        return res
          .status(200)
          .json({ success: true, message: "Task updated successfuly." });
      } else {
        return res.status(400).json({
          success: false,
          message: "Access Denied: You are not authorized.",
        });
      }
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

exports.deleteTask = async (req, res) => {
  try {
    const { id } = req.params;
    const task = await Task.query().findById(id).withGraphFetched("project");
    if (!task) {
      return res
        .status(400)
        .json({ success: false, message: "Task does not exist !" });
    } else {
      if (req.user.id === task.project.user_id) {
        await Task.query().deleteById(id);
        await updateProjectWhenUpdateTask(task.project_id);
        return res
          .status(200)
          .json({ success: true, message: "Task deleted successfuly." });
      } else {
        return res.status(400).json({
          success: false,
          message: "Access Denied: You are not authorized.",
        });
      }
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

async function updateProjectWhenUpdateTask(project_id) {
  const sameProjectIdTasks = await Task.query().where("project_id", project_id);
  const notCompletedTask = sameProjectIdTasks.find(
    (task) => task.completed === false
  );
  if (notCompletedTask === undefined) {
    await Project.query().findById(project_id).patch({
      completed: true,
    });
  } else {
    await Project.query().findById(project_id).patch({
      completed: false,
    });
  }
}
