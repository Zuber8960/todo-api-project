exports.error = async (req, res) => {
    return res.status(404).json({success : false, message : "Invalid request"});
}