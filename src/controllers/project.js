const Project = require("../db/models/project");

exports.createProject = async (req, res) => {
  try {
    const { title, description } = req.body;
    const data = await Project.query().insert({
      title: title,
      description: description,
      completed: false,
      user_id: req.user.id,
    });
    return res.status(200).json({ success: true, data: data });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

exports.getAllProjects = async (req, res) => {
  try {
    const projects = await Project.query().where({ user_id: req.user.id }).withGraphFetched("task");
    if (projects.length === 0) {
      return res.status(400).json({
        success: false,
        message: "No Project Found: Please create project.",
      });
    } else {
      return res.status(200).json({ success: true, data: projects });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, message: err.message });
  }
};

exports.getAProject = async (req, res) => {
  const { id } = req.params;
  try {
    const data = await Project.query().findById(id).withGraphFetched("task");
    if (!data) {
      return res.status(400).json({
        success: false,
        message:
          "Invalid Project ID: The project you are trying to access does not exist.",
      });
    } else {
      if (req.user.id === +data.user_id) {
        return res.status(200).json({ success: true, data: data });
      } else {
        return res.status(400).json({
          success: false,
          message: "Access Denied: You are not authorized.",
        });
      }
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, message: err.message });
  }
};

exports.deleteAProject = async (req, res) => {
  const { id } = req.params;
  try {
    const data = await Project.query().findById(id);
    if (!data) {
      return res.status(400).json({
        success: false,
        message:
          "Invalid Project ID: The project you are trying to access does not exist.",
      });
    } else {
      if (req.user.id === data.user_id) {
        await Project.query().deleteById(id);
        return res
          .status(200)
          .json({ success: true, message: "Project deleted successfully." });
      } else {
        return res.status(400).json({
          success: false,
          message: "Access Denied: You are not authorized.",
        });
      }
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, message: err.message });
  }
};

exports.updateProject = async (req, res) => {
  const { id } = req.params;
  const { title, description, completed } = req.body;
  try {
    const data = await Project.query().findById(id);
    if (!data) {
      return res.status(400).json({
        success: false,
        message:
          "Invalid Project ID: The project you are trying to access does not exist.",
      });
    } else {
      if (req.user.id === data.user_id) {
        await Project.query().findById(id).patch({
          title: title,
          description: description,
          completed: completed,
        });
        return res
          .status(200)
          .json({ success: true, message: "Project updated successfully." });
      } else {
        return res.status(400).json({
          success: false,
          message: "Access Denied: You are not authorized.",
        });
      }
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, message: err.message });
  }
};
