const User = require("../db/models/user");
const bcrypt = require("bcrypt");
const generateToken = require("../middleware/auth").generateToken;

exports.createUser = async (req, res) => {
  const saltRounds = 10;
  try {
    const { name, email, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    const user = await User.query().insert({
      username: name,
      email: email,
      password: hashedPassword,
    });
    const obj = {
      username: user.username,
      email: user.email
    }
    return res.status(200).json({ success: true, data: obj });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

exports.getUserProfile = async (req, res) => {
  try {
    const { username } = req.params;
    const data = await User.query().findOne("username", username);
    if (data === undefined) {
      return res
        .status(400)
        .json({ success: false, message: "User does not exist !" });
    } else {
      const user = {
        username: data.username,
        email: data.email
      }
      return res.status(200).json({ success: true, data: user });
    }
  } catch (err) {
    console.error(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

exports.userLogin = async (req, res) => {
  try {
    const { email, password } = req.body;
    if(!email || !password){
      return res.status(400).json({success: false, message: "Email and Password Required"})
    }
    const data = await User.query().findOne("email", email);
    if (data === undefined) {
      return res
        .status(400)
        .json({ success: false, message: "User does not exist !" });
    } else {
      const isPasswordMatched = await bcrypt.compare(password, data.password);
      if (isPasswordMatched) {
        const obj = { username: data.username, id: data.id };
        const token = generateToken(obj);
        const user = {
          username: data.username,
          email: data.email,
          token: token
        }
        return res
          .status(200)
          .json({ success: true, data: user});
      } else {
        return res.status(400).json({
          success: false,
          message: "Incorrect Password: The password you entered is incorrect.",
        });
      }
    }
  } catch (err) {
    console.error(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};

exports.updateUserProfile = async (req, res) => {
  try {
    const { username } = req.params;
    const { email } = req.body;
    if(!email){
      return res.status(400).json({ success: false, message: "Email is required." })
    }
    const user = await User.query().findOne({ username: username });
    if (user === undefined) {
      return res
        .status(400)
        .json({ success: false, message: "User does not exist !" });
    } else {
      if (req.user.id === user.id) {
        const checkEmailIsAnotherUserEmail = await User.query()
          .findOne("email", email)
          .whereNot("id", user.id);

        if (checkEmailIsAnotherUserEmail === undefined) {
          await User.query().findOne({ username: username }).patch({
            email: email,
          });
          return res
            .status(200)
            .json({ success: true, message: "User updated successfully." });
        } else {
          return res.status(400).json({
            success: false,
            message:
              "Email Already Exists: The email address you provided is already registered.",
          });
        }
      } else {
        return res.status(400).json({
          success: false,
          message: "Access Denied: You are not authorized.",
        });
      }
    }
  } catch (err) {
    console.error(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};
