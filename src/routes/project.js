const express = require("express");
const Router = express.Router();

const projectController = require("../controllers/project");
const middleware = require("../middleware/auth");

Router.post("/", middleware.authentication, projectController.createProject);
Router.get("/", middleware.authentication, projectController.getAllProjects);
Router.get("/:id", middleware.authentication, projectController.getAProject);
Router.put("/:id", middleware.authentication, projectController.updateProject);
Router.delete("/:id", middleware.authentication, projectController.deleteAProject);

module.exports = Router;
