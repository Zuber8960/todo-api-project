const express = require("express");
const Router = express.Router();

const userController = require("../controllers/user");
const middleware = require("../middleware/auth");

Router.post("/", userController.createUser);
Router.get("/:username", userController.getUserProfile);
Router.post("/login", userController.userLogin);
Router.put("/:username", middleware.authentication, userController.updateUserProfile);

module.exports = Router;