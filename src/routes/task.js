const express = require("express");

const Router = express.Router();

const taskController = require("../controllers/task");
const middleware = require("../middleware/auth");


Router.get("/", middleware.authentication,taskController.getAllTasks);
Router.post("/", middleware.authentication,taskController.createTask);
Router.get("/:id", middleware.authentication, taskController.getATask);
Router.put("/:id", middleware.authentication, taskController.updateTask);
Router.delete("/:id", middleware.authentication, taskController.deleteTask);

module.exports = Router;
