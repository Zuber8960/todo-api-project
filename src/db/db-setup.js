const knex = require("knex");
const knexfile = require("../../knexfile");

const { Model } = require("objection");

function setupDb() {
  try {
    const db = knex(knexfile.development);
    Model.knex(db);
    console.log("Database connection established successfully.");
  } catch (err) {
    console.error(err);
    console.error("Error setting up the database connection.");
  }
}

module.exports = setupDb;
