const { Model } = require("objection");

class Project extends Model {
  static get tableName() {
    return "projects";
  }
  static get relationMappings() {
    const User = require("./user");
    const Task = require("./task");
    // console.log("this is log in project model");

    return {
      user: {
        relation: Model.HasOneRelation,
        modelClass: User,
        join: {
          from: "projects.user_id",
          to: "users.id",
        },
      },
      task: {
        relation: Model.HasManyRelation,
        modelClass: Task,
        join: {
          from: "projects.id",
          to: "tasks.project_id",
        },
        cascadeDelete: true,
      },
    };
  }
}

module.exports = Project;
