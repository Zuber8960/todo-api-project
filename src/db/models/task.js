const { Model } = require('objection');

class Task extends Model {
  static get tableName() {
    return 'tasks';
  }
  static get relationMappings() {
    const Project = require("./project");
    return {
      project: {
        relation: Model.HasOneRelation,
        modelClass: Project,
        join: {
          from: "tasks.project_id",
          to: "projects.id",
        },
      },
    };
  }
}

module.exports = Task;