exports.up = function (knex) {
  return knex.schema.createTable("users", (table) => {
    table.increments("id");
    table.string("username").notNullable().unique();
    table.string("email").notNullable().unique();
    table.string("password").notNullable();
    table.timestamps(true, true);
  })
  .createTable("projects", (table) => {
    table.increments("id");
    table.string("title").notNullable();
    table.string("description").notNullable();
    table.boolean("completed").notNullable();
    table.integer("user_id")
    .references("id")
    .inTable("users");
    table.timestamps(true, true);
  })
  .createTable("tasks", (table) => {
    table.increments("id");
    table.string("title").notNullable();
    table.boolean("completed").notNullable();
    table.integer("project_id")
    .references("id")
    .inTable("projects")
    .onDelete("CASCADE");
    table.timestamps(true, true);
  })
};


exports.down = function (knex) {
  return knex.schema.dropTableIfExists("tasks")
  .dropTableIfExists("projects")
  .dropTableIfExists("users")
};