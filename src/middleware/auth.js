const jwt = require("jsonwebtoken");
const User = require("../db/models/user");
const config = require("../../config")

exports.authentication = async (req, res, next) => {
  try {
    const token = req.header("Authorization");
    const data = jwt.verify(token, config.SECRETKEY);
    const user = await User.query().findById(data.id);
    req.user = user;
    next();
  } catch (err) {
    console.error(err);
    return res.status(500).json({ success: false, message: err.message });
  }
};



exports.generateToken = function (obj) {
  return jwt.sign(obj, config.SECRETKEY);
}