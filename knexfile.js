const { knexSnakeCaseMappers } = require("objection");

const { DBCONFIG } = require("./config");

module.exports = {
  development: {
    client: "postgresql",
    connection: {
      connectionString: DBCONFIG,
      ssl: { rejectUnauthorized: false },
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: "./src/db/migrations",
    },
    ...knexSnakeCaseMappers,
  },
};
